# -*- coding: utf-8 -*-
import os
from os.path import join, dirname
from setuptools import setup, find_packages, Command
import sys

VERSION = "1.0.0"

class CleanCommand(Command):
    """Custom clean command to tidy up the project root."""
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        os.system('for /d /r %i in (*egg-info*) do @rmdir /s /q %i')
        os.system('IF EXIST dist rmdir /S /Q dist')

setup(
    name='AtlasJobFeedExport',
    description="JobFeedExport from Atlas",
    version=VERSION,
    packages=find_packages(
        where='.',
        exclude=['contrib', 'docs', 'tests'],
    ),
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    zip_safe=False,
    cmdclass={'clean': CleanCommand,},
    install_requires=['pika', 'functools', 'datetime', 'logging','pytest-runner','pytest','pytest-cov', 'pytest-html','boto3','fake-awsglue'],
)
