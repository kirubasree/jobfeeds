from pyspark.sql import SparkSession
import json
import argparse
import boto3
from botocore.exceptions import ClientError
import base64
from datetime import datetime
from datetime import timedelta
import gzip
from awsglue.utils import getResolvedOptions
import sys
from io import BytesIO
from gzip import GzipFile
import shutil
from pyspark.sql.types import StringType,StructType
from pyspark.sql.functions import udf
import awsglue


SNOWFLAKE_SOURCE_NAME = "net.snowflake.spark.snowflake"

class SuperFeedExport:

    def __init__(self):
        print('[INFO] \"Started the Feed Generation\"')

    def load_configuration(self):
        try:

            args = awsglue.utils.getResolvedOptions(sys.argv,
                              ['JOB_NAME',
                              'application_name',
                              'environment_name',
                              'configuration_name',
                              'client_configuration_version'])


            application_name=args['application_name']
            environment_name=args['environment_name']
            configuration_name=args['configuration_name']
            client_configuration_version=args['client_configuration_version']
            
            client = boto3.client('appconfig')
            
            response = client.get_configuration(
                Application=application_name,
                Environment=environment_name,
                Configuration=configuration_name,
                ClientId='test',
                ClientConfigurationVersion=client_configuration_version
            )

            config = json.loads(response['Content'].read().decode("utf-8"))
            print("[INFO] \"Loaded Configuration from AppConfig\"");
            print("[INFO] \"Getting the Snowflake credentials from Secret Manager\"");
            secret_name = str(config['SnowflakeAccountSecretmanager'])
            secret = self.get_secret(secret_name)
            if(secret is None):
                raise Exception("[ERROR] \"SecretManagerError\" \"Error in getting the Snowflake Credentials from Secret manager\"")
            snowflakeAccount = json.loads(secret)

            self.clientName = str(config['ClientName'])
            self.feedType = str(config['FeedType'])
            self.feedDelay = int(config['FeedDelay'])
            self.feedSpan = int(config['Span'])
            self.dateType = str(config['DateType'])
            self.snowflakeQuery = str(config['FeedQuery'])
            self.geoFeedFileName = str(config['GeoFeedFileNameTemplate'])
            self.stateFeedFileName = str(config['StateFeedFileNameTemplate'])
            self.feedTempPath = str(config['FeedTempPath'])
            self.geoSFTPPath = str(config['GeoSFTPPath'])
            self.stateSFTPPath = str(config['StateSFTPPath'])
            self.sparkSQLQuery = str(config['SparkSQLQuery'])
            self.s3DataFrames = str(config['s3DataFrames'])

            if self.feedType.lower() == 'delta':
                self.feedStartDate = datetime.strptime(str((datetime.now() + timedelta(days = -self.feedDelay)).date()) + ' ' + str(config['BaseHour']),"%Y-%m-%d %H:%M")
                self.feedEndDate = self.feedStartDate + timedelta(hours=self.feedSpan)
                while self.feedEndDate + timedelta(hours=self.feedSpan) < datetime.now() + timedelta(days=-self.feedDelay):
                    self.feedStartDate = self.feedEndDate
                    self.feedEndDate = self.feedStartDate + timedelta(hours=self.feedSpan)

            if self.feedType.lower() == 'historic':
                self.feedStartDate = datetime.strptime(str(config['FeedStartDate']),"%Y-%m-%d")
                self.feedEndDate = datetime.strptime(str(config['FeedEndDate']),"%Y-%m-%d")

            if self.feedType.lower() == 'baseline':
                self.feedStartDate = datetime.strptime(str(config['FeedStartDate']),"%Y-%m-%d")
                self.feedEndDate = datetime.strptime(str((datetime.now() + timedelta(days = -self.feedDelay)).date()) + ' ' + '00:00',"%Y-%m-%d %H:%M")

            self.sfOptions = {
                "sfURL" : str(config['SnowflakeURL']),
                "sfAccount" : str(config['SnowflakeAccount']),
                "sfUser" : str(snowflakeAccount["USER"]),
                "sfPassword" : str(snowflakeAccount["PASSWORD"]),
                "sfWarehouse" :  str(config["SnowflakeWarehouse"]),
                "sfDatabase" : str(config['SnowflakeDatabase']),
                "sfSchema" : str(config['SnowflakeSchema']),
                "sfRole" : str(config['SnowflakeRole']),
                }
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error loading the configuration file\" - " + str(err))
            raise Exception(err)

    def get_secret(self,secret_name):

        region_name = "us-east-1"
        #Create a Secrets Manager client
        session = boto3.session.Session() #profile_name='BGTDevPowerUser'
        client = session.client(
            service_name='secretsmanager',
            region_name=region_name
        )
        # In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
        # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
        # We rethrow the exception by default.
        try:
            get_secret_value_response = client.get_secret_value(
                SecretId=secret_name
            )
        except ClientError as e:
            print('[ERROR] \"SecretManagerError\" \"Error in getting Snowflake credentials from Secret Manager\" - ' + str(e))
            if e.response['Error']['Code'] == 'DecryptionFailureException':
                # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'InternalServiceErrorException':
                # An error occurred on the server side.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'InvalidParameterException':
                # You provided an invalid value for a parameter.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'InvalidRequestException':
                # You provided a parameter value that is not valid for the current state of the resource.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
            elif e.response['Error']['Code'] == 'ResourceNotFoundException':
                # We can't find the resource that you asked for.
                # Deal with the exception here, and/or rethrow at your discretion.
                raise e
        else:
            # Decrypts secret using the associated KMS CMK.
            # Depending on whether the secret is a string or binary, one of these fields will be populated.
            if 'SecretString' in get_secret_value_response:
                return  get_secret_value_response['SecretString']
            else:
                return base64.b64decode(get_secret_value_response['SecretBinary'])

    def copyFileToS3(self,source_bucket,target_bucket,source_prefix,target_prefix):
        try:
            client = boto3.client('s3')
            copy_source_file = {'Bucket': source_bucket, 'Key': source_prefix}
            client.copy(CopySource=copy_source_file, Bucket=target_bucket, Key=target_prefix)
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occured while copying the file\" from " + source_bucket + source_prefix + " to " + target_bucket + target_prefix + " - " + str(err))
            raise Exception(err)

    def renameOutputFile(self,source_bucket,target_bucket,source_prefix,target_prefix):
        try:
            client = boto3.client('s3')
            response = client.list_objects_v2(Bucket = source_bucket,Prefix = source_prefix,MaxKeys=100)
            print('[INFO] \"Compressing the Exported Feed File\"')
            for obj in response.get('Contents', []):
                if obj['Key'].endswith(".txt"):
                    print(obj['Key'])
                    copy_source_file = {'Bucket': source_bucket, 'Key': obj['Key']}
                    client.copy(CopySource=copy_source_file, Bucket=target_bucket, Key=target_prefix)
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occured while renaming the file\" from " + source_bucket + source_prefix + " to " + target_bucket + target_prefix + " - " + str(err))
            raise Exception(err)

    def compressFile(self,source_bucket,target_bucket,source_prefix,target_prefix,fileName):
        try:
            client = boto3.client('s3')
            response = client.list_objects_v2(Bucket = source_bucket,Prefix = source_prefix,MaxKeys=100)
            for obj in response.get('Contents', []):
                if obj['Key'].endswith(".txt"):
                    #print('[INFO] \"Compressing the Exported Feed File\" - ' + obj['Key'])
                    s3 = boto3.resource('s3')
                    bucket = s3.Bucket(source_bucket)
                    obj = bucket.Object(key=obj['Key'])
                    response = obj.get()['Body']
                    gz_body = BytesIO()
                    with gzip.GzipFile(fileobj=gz_body, mode='wb') as gz:
                        shutil.copyfileobj(response, gz)
                    gz_body.seek(0)
                    #GzipFile has written the compressed bytes into our gz_body
                    s3upload = boto3.client('s3')
                    s3upload.upload_fileobj(
                        Fileobj = gz_body,
                        Bucket=target_bucket,
                        Key= target_prefix  + fileName + '.gz',
                        ExtraArgs= {'ContentType':'application/json', 'ContentEncoding':'gzip'}
                    )
                    print('[INFO] \"Compressed the Exported Feed File\" - ' + target_prefix  + fileName + '.gz',)
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occured while Compressing the file\" from " + source_bucket + source_prefix + " to " + target_bucket + target_prefix + " - " + str(err))
            raise Exception(err)

    def querySnowflake(self,sfOptions,snowflakeQuery):
        try:
            print('\"Executing Snowflake Query\"');
            result_df = self.spark.read.format(SNOWFLAKE_SOURCE_NAME) \
            .options(**sfOptions) \
            .option("query", snowflakeQuery) \
            .option("encoding", "UTF-8") \
            .load()
            return result_df
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred while executing the snowflake feed export query\" - " + str(err))
            raise Exception(err)

    def createSparkSession(self):
        try:
            print('[INFO] \"Initializing Spark Session\"')
            self.spark = SparkSession.builder.config("spark.jars.packages","net.snowflake:snowflake-jdbc:3.0.14,net.snowflake:spark-snowflake_2.11:2.7.2-spark_2.2,io.delta:delta-core_2.11:0.6.0").getOrCreate()
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred while creating the Spark Session\" - " + str(err))
            raise Exception(err)

    def writeDataFrameToFile(self,df_out):
        try:
            print('[INFO] \"Writing the query results to the temp file in s3\" - ' + self.feedTempPath)
            df_out.coalesce(1).write.format("text").option("header", "false").option("encoding", "UTF-8").mode("overwrite").save(self.feedTempPath)
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred while writing the query result to file\" - " + str(err))
            raise Exception(err)

    def dataFrameToJSON(self,dfvalue):
        try:
            count = dfvalue.count()
            print(count)
            if(count > 1):
                return dfvalue.rdd.zipWithIndex().map(lambda row : ("{\n \t \""+ "Jobs" + "\": [" + row[0][0] + ",", ) if(row[1]==0) else ((row[0][0] + "] \n \t }", ) if (row[1]== count-1) else ((row[0][0] +","),) )).toDF()
            else:
                return dfvalue.rdd.zipWithIndex().map(lambda row : ("{\n \t \""+ "Jobs" + "\": [" + row[0][0] + "] \n \t }", ) if(row[1]==0) else ((row[0][0] + "] \n \t }", ) if (row[1]== count-1) else ((row[0][0] +","),) )).toDF()
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred while converting the dataframe object to JSON array\" - " + str(err))
            raise Exception(err)

    def createS3Dataframe(self,):
        try:
            print("\"Creating s3 Temp Views\"")
            for path in self.s3DataFrames.split("|"):
                s3df = self.spark.read.format("delta").load(path.split(";")[0])
                s3df.createOrReplaceTempView(path.split(";")[1])
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred while creating the dataframe from s3 deltaIO\" - " + str(err))
            raise Exception(err)

    def registerUDF(self):
        try:
            def parseJSON(*args):
                null = None
                j = json.loads(args[0],encoding='UTF-8')
                for i in range(1,len(args),2):
                    j[args[i]] = args[i+1]
                return json.dumps(j, indent=2, ensure_ascii =False)
            print('[INFO] \"Registering the Spark UDF Functions\"')
            self.spark.udf.register('json_udf',parseJSON)
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred while registering the UDF Function in Spark session\" - " + str(err))
            raise Exception(err)

    def generateFeed(self,startDate,EndDate,query,geoFileName,stateFileName):
        try:
            print('[INFO] \"Started Generation of the feed for the client\" - \"' + self.clientName+'\"');
            snowflake_df = self.querySnowflake(self.sfOptions,query)
            print('[INFO] \"Snowflake Query Execution Completed for the date range\" from ' + startDate + ' to ' + EndDate + ' \"Job Count\" '+ str(snowflake_df.count()));
            self.createS3Dataframe()
            snowflake_df.createOrReplaceTempView("SnowflakeData")
            print('[INFO] \"Executing the Spark SQL Query\"')
            df = self.spark.sql(self.sparkSQLQuery)
            print('[INFO] \"Spark SQL Query execution completed\" \"Job Count\" ' + str(df.count()))
            df.createOrReplaceTempView("Output")
            result = df.select("Geo").distinct().rdd.map(tuple).collect()
            print('[INFO] \"Distinct Geo''s in the Spark SQL Query result\" - ' + str(result))
            for geo in result:
                if geo[0] != None:
                    print("[INFO] \"select * from Output where geo = '" + geo[0] +"'\"")
                    df_geo = self.spark.sql("select * from Output where geo = '" + geo[0] + "'")
                    jobCount = self.spark.sql("select count(*) from Output where geo = '" + geo[0] + "'").collect()[0][0]
                    df_json = self.dataFrameToJSON(df_geo)
                    self.writeDataFrameToFile(df_json)
                    print('[INFO] \"Filtering the result for the Geo\" - ' + str(geo[0]) + ' \"Job Count\" - ' + str(jobCount))
                    ftpPath = self.geoSFTPPath.replace('{Geo}',geo[0].replace(" ",""))
                    self.compressFile(self.feedTempPath.split('/')[2],ftpPath.split('/')[2],'/'.join(self.feedTempPath.split('/')[3:]),'/'.join(ftpPath.split('/')[3:]),geoFileName.replace('{Geo}',geo[0].replace(" ","")))
            result = df.select("Geo","State").distinct().rdd.map(tuple).collect()
            print(result)
            print('[INFO] \"Distinct Geo''s and State in the Spark SQL Query result\" - ' + str(result))
            for geo in result:
                if geo[0] and geo[1] != None:
                    print("[INFO] \"select * from Output where geo = '" + geo[0] + "' and state = '" + geo[1] + "'\"")
                    df_geo = self.spark.sql("select * from Output where geo = '" + geo[0] + "' and state = '" + geo[1] + "'")
                    jobCount = self.spark.sql("select count(*) from Output where geo = '" + geo[0] + "' and state = '" +  geo[1] + "'").collect()[0][0]
                    df_json = self.dataFrameToJSON(df_geo)
                    self.writeDataFrameToFile(df_json)
                    print('[INFO] \"Filtering the result for the\" \"Geo\" ' + str(geo[0]) + ' \"State\" ' + str(geo[1]) + ' \"Job Count\" ' + str(jobCount))
                    ftpPath = self.stateSFTPPath.replace('{Geo}',geo[0].replace(" ","")).replace('{State}',geo[1].replace(" ",""))
                    self.compressFile(self.feedTempPath.split('/')[2],ftpPath.split('/')[2],'/'.join(self.feedTempPath.split('/')[3:]),'/'.join(ftpPath.split('/')[3:]),stateFileName.replace('{Geo}',geo[0].replace(" ","")).replace('{State}',geo[1].replace(" ","")))
        except Exception as err:
            print("[ERROR] \""+self.clientName+"\" \"Error occurred in generation of the Feed for the client\" - + " + self.clientName + " - " + str(err))
            raise Exception(err)

    def driver(self):
        self.load_configuration()
        self.createSparkSession()
        self.registerUDF()
        if self.feedType.lower() == 'delta':
            if(self.dateType.lower() == 'date'):
                startDate = str(self.feedStartDate.date())
                endDate = str(self.feedEndDate.date())
                dateFormat = "%Y-%m-%d"
            else:
                startDate = str(self.feedStartDate) + '.000'
                endDate = str(self.feedEndDate - timedelta(seconds=1)) + '.997' #str(self.feedEndDate) + '.997'
                dateFormat = "%Y-%m-%d %H:%M:%S.%f"
            query = self.snowflakeQuery.replace('{StartDate}',"'" + str(startDate) + "'").replace('{EndDate}',"'" + str(endDate) + "'")
            geoFileName = self.geoFeedFileName.replace('{FeedStartDate}',datetime.strptime(startDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedEndDate}',datetime.strptime(endDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedStartHour}',datetime.strptime(startDate,dateFormat).strftime("%H"))\
                    .replace('{FeedEndHour}',datetime.strptime(endDate,dateFormat).strftime("%H"))
            stateFileName = self.stateFeedFileName.replace('{FeedStartDate}',datetime.strptime(startDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedEndDate}',datetime.strptime(endDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedStartHour}',datetime.strptime(startDate,dateFormat).strftime("%H"))\
                    .replace('{FeedEndHour}',datetime.strptime(endDate,dateFormat).strftime("%H"))
            self.generateFeed(startDate,endDate,query,geoFileName,stateFileName)

        if self.feedType.lower() in ('baseline','historic'):
            tempEndDate = self.feedStartDate + timedelta(days=self.feedSpan)
            lastIteration = 0
            if(tempEndDate > self.feedEndDate and lastIteration == 0):
                tempEndDate = self.feedEndDate
                lastIteration = 1
            while(tempEndDate <= self.feedEndDate and self.feedStartDate <= self.feedEndDate):
                while(tempEndDate.year != self.feedStartDate.year):
                    tempEndDate = tempEndDate + timedelta(days=-1)
                if(self.dateType.lower() == 'date'):
                    startDate = str(self.feedStartDate.date())
                    endDate = str(tempEndDate.date())
                    dateFormat = "%Y-%m-%d"
                else:
                    startDate = str(self.feedStartDate) + '.000'
                    endDate = str((tempEndDate).date()) + ' ' + '23:59:59.997'
                    dateFormat = "%Y-%m-%d %H:%M:%S.%f"
                query = self.snowflakeQuery.replace('{StartDate}',"'" + str(startDate) + "'").replace('{EndDate}',"'" + str(endDate) + "'")
                geoFileName = self.geoFeedFileName.replace('{FeedStartDate}',datetime.strptime(startDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedEndDate}',datetime.strptime(endDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedStartHour}',datetime.strptime(startDate,dateFormat).strftime("%H"))\
                    .replace('{FeedEndHour}',datetime.strptime(endDate,dateFormat).strftime("%H"))
                stateFileName = self.stateFeedFileName.replace('{FeedStartDate}',datetime.strptime(startDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedEndDate}',datetime.strptime(endDate,dateFormat).strftime("%Y%m%d"))\
                    .replace('{FeedStartHour}',datetime.strptime(startDate,dateFormat).strftime("%H"))\
                    .replace('{FeedEndHour}',datetime.strptime(endDate,dateFormat).strftime("%H"))
                self.generateFeed(startDate,endDate,query,geoFileName,stateFileName)
                self.feedStartDate = tempEndDate + timedelta(days=1)
                tempEndDate = self.feedStartDate + timedelta(days=self.feedSpan)
                if(tempEndDate > self.feedEndDate and lastIteration == 0):
                    tempEndDate = self.feedEndDate
                    lastIteration = 1

if __name__ == '__main__':
    export  = SuperFeedExport()
    export.driver()