import sys, os
import requests
from pyspark.sql import SparkSession
from mock import patch
import boto3
from botocore.exceptions import ClientError
import base64
myPath = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.insert(0, myPath + '/src')
import shutil
import unittest
from atlas_jobfeed_export import SuperFeedExport
from awsglue.utils import getResolvedOptions
import json


sampleconfig = dict(ClientName="Daily Vacancy Feed",
                    FeedQuery="WITH vacancy as  ( SELECT * FROM \"BGT_DATA_34\".\"PUBLIC\".\"F_BGT_VACANCY\" v WHERE v.FIRST_ACTIVE_DATE_KEY IN (SELECT DATE_KEY FROM \"BGT_DATA_34\".\"PUBLIC\".\"D_DATE\" WHERE  DATE BETWEEN {StartDate} AND {EndDate}) ), sourceRank as ( Select 'corporatesite' as SourceName, 10 as Priority UNION Select 'jobboard' as SourceName, 20 as Priority UNION Select 'jobfeed' as SourceName, 30 as Priority UNION Select 'socialfeed' as SourceName, 40 as Priority UNION Select 'newspaper' as SourceName, 50 as Priority UNION Select NULL as SourceName, 60 as Priority ), posting_map as  ( SELECT VACANCY_KEY as posting_map_key ,temp.POSTING_KEY ,object_construct('SOURCE_ID',IFF(Priority IS NULL,parse_json('60'),Priority),'SOURCE_NAME',IFF(temp.SOURCE_NAME IS NULL,'',temp.SOURCE_NAME))SOURCE ,IFF(temp.SOURCE_DOMAIN IS NULL,'',temp.SOURCE_DOMAIN)as SOURCE_DOMAIN ,IFF(temp.ORIGINAL_URL IS NULL,'',temp.ORIGINAL_URL) as ORIGINAL_URL FROM(  (SELECT fttable.VACANCY_KEY \t,fttable.ftvalue AS POSTING_KEY \t,CRAWLING_ACTIVITY_SOURCE: crawling_activity_source_type::string AS SOURCE_NAME \t,CRAWLING_ACTIVITY_SOURCE: crawling_activity_source_name::VARCHAR (2000) AS SOURCE_DOMAIN \t,post.ORIGINAL_URL \t,s.Priority \t,ROW_NUMBER() OVER ( \t\tPARTITION BY fttable.VACANCY_KEY ORDER BY s.Priority \t\t\t,post.POSTING_KEY,d.DATE ASC \t\t) AS R FROM ( \tSELECT v.VACANCY_KEY,ft.value AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.POSTING_KEY, OUTER => TRUE)) ft \t) fttable LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"F_BGT_POSTING\" post ON post.POSTING_KEY =  fttable.ftvalue LEFT JOIN SourceRank s ON s.SourceName = CRAWLING_ACTIVITY_SOURCE:crawling_activity_source_type::string INNER JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_DATE\" d on d.DATE_KEY = post.FIRST_ACTIVE_DATE_KEY) ) temp where R =1 ), certs_map as ( SELECT fttable.VACANCY_KEY AS CERTS_MAP_KEY \t,array_agg(object_construct('CERTIFICATION',IFF(certs.CERTIFICATION_NAME_EN_US IS NULL,'',certs.CERTIFICATION_NAME_EN_US::VARCHAR (8000)), 'VERSION', '3.4.1')) CERTIFICATION_NAME_EN_US FROM ( \tSELECT v.VACANCY_KEY,ft.value AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.BGT_CERTIFICATION_030401_KEY, OUTER => TRUE)) ft \t) fttable LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_BGT_CERTIFICATION_030401\" certs ON certs.BGT_CERTIFICATION_030401_KEY =  fttable.ftvalue GROUP BY fttable.VACANCY_KEY ), edu_map_min as  ( SELECT ftable.VACANCY_KEY AS EDU_MAP_KEY \t,array_construct(object_construct('MIN_DEGREE_LEVEL',IFF(edu.EDUCATION_LEVEL_CODE IS NULL,'',edu.EDUCATION_LEVEL_CODE), 'VERSION','3.4','NAME',IFF(edu.EDUCATION_LEVEL_EN_US IS NULL,'',edu.EDUCATION_LEVEL_EN_US)))EDUCATION_LEVEL_CODE_MIN_DEGREE_LEVEL FROM  ( \tSELECT v.VACANCY_KEY,min(ft.value) AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.EDUCATION_LEVEL_KEY, OUTER => TRUE)) ft    GROUP BY v.VACANCY_KEY \t) ftable LEFT JOIN  \"BGT_DATA_34\".\"PUBLIC\".\"D_EDUCATION_LEVEL\" edu ON edu.EDUCATION_LEVEL_KEY =  ftable.ftvalue ), edu_map_max as  ( SELECT ftable.VACANCY_KEY AS EDU_MAP_KEY \t,array_construct(object_construct('MAX_DEGREE_LEVEL',IFF(edu.EDUCATION_LEVEL_CODE IS NULL,'',edu.EDUCATION_LEVEL_CODE), 'VERSION','3.4','NAME',IFF(edu.EDUCATION_LEVEL_EN_US IS NULL,'',edu.EDUCATION_LEVEL_EN_US)))EDUCATION_LEVEL_CODE_MAX_DEGREE_LEVEL FROM  ( \tSELECT v.VACANCY_KEY,max(ft.value) AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.EDUCATION_LEVEL_KEY, OUTER => TRUE)) ft   GROUP BY v.VACANCY_KEY \t) ftable LEFT JOIN  \"BGT_DATA_34\".\"PUBLIC\".\"D_EDUCATION_LEVEL\" edu ON edu.EDUCATION_LEVEL_KEY =  ftable.ftvalue ), edu_map as  ( SELECT ftable.VACANCY_KEY AS EDU_MAP_KEY \t,array_agg(IFF(edu.EDUCATION_LEVEL_CODE IS NULL,'',edu.EDUCATION_LEVEL_CODE))EDUCATION_LEVEL_CODE_EDUCATION_LEVEL FROM ( \tSELECT v.VACANCY_KEY,ft.value AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.EDUCATION_LEVEL_KEY, OUTER => TRUE)) ft \t) ftable LEFT JOIN  \"BGT_DATA_34\".\"PUBLIC\".\"D_EDUCATION_LEVEL\" edu ON edu.EDUCATION_LEVEL_KEY =  ftable.ftvalue GROUP BY ftable.VACANCY_KEY ), cip_map as ( SELECT fttable.VACANCY_KEY AS CIP_MAP_KEY \t   ,array_agg(object_construct('CIP_CODE',IFF(cip.CIP6_CODE IS NULL,'',cip.CIP6_CODE),'NAME',IFF(cip.cip6_title_en_us IS NULL,'',cip.cip6_title_en_us),'VERSION','2010'))CIP6_CODE FROM  ( \tSELECT v.VACANCY_KEY,ft.value AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.CIP_2010_KEY, OUTER => TRUE)) ft \t) fttable LEFT JOIN  \"BGT_DATA_34\".\"PUBLIC\".\"D_CIP_2010\" cip ON cip.CIP_2010_KEY =  fttable.ftvalue GROUP BY fttable.VACANCY_KEY ), nqf_req_map as ( SELECT fttable.VACANCY_KEY AS NQF_MAP_KEY \t   ,array_agg(object_construct('NQF_REQUIRED',IFF(nqf.NQF_ID IS NULL,'',nqf.NQF_ID),'NAME',IFF(nqf.NQF_LEVEL IS NULL,'',nqf.NQF_LEVEL),'VERSION','2015'))NQF_ID_required FROM ( \tSELECT v.VACANCY_KEY,min(ft.value) AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.UK_NQF_2015_KEY, OUTER => TRUE)) ft \tgroup by v.VACANCY_KEY \t) fttable LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_UK_NQF_2015\" nqf ON nqf.UK_NQF_2015_KEY =  fttable.ftvalue GROUP BY fttable.VACANCY_KEY ), nqf_pre_map as ( SELECT fttable.VACANCY_KEY AS NQF_MAP_KEY \t   ,array_agg(object_construct('NQF_PREFERRED',IFF(nqf.NQF_ID IS NULL,'',nqf.NQF_ID),'NAME',IFF(nqf.NQF_LEVEL IS NULL,'',nqf.NQF_LEVEL),'VERSION','2015'))NQF_ID_PREFERRED FROM ( \tSELECT v.VACANCY_KEY,ft.value AS ftvalue,ROW_NUMBER() OVER ( \t\tPARTITION BY v.VACANCY_KEY ORDER BY ft.value ASC \t\t) AS R \tFROM vacancy v \t\t,TABLE (flatten(input => v.UK_NQF_2015_KEY, OUTER => TRUE)) ft \t) fttable LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_UK_NQF_2015\" nqf ON nqf.UK_NQF_2015_KEY =  fttable.ftvalue where fttable.R != 1 GROUP BY fttable.VACANCY_KEY ), skill_map as  ( SELECT fttable.VACANCY_KEY AS SKILL_MAP_KEY \t,array_agg(object_construct('SKILL_NAME', IFF(skill.SKILL_NAME_EN_US IS NULL,'', skill.SKILL_NAME_EN_US), 'VERSION', '3.4', 'SKILL_CLUSTER_NAME', IFF(skill.SKILL_CLUSTER_NAME_EN_US IS NULL,'', concat(skill.SKILL_CLUSTER_FAMILY_NAME_EN_US,':',skill.SKILL_CLUSTER_NAME_EN_US)))) SKILL FROM ( \tSELECT v.VACANCY_KEY,ft.value AS ftvalue \tFROM vacancy v \t\t,TABLE (flatten(input => v.BGT_SKILL_030401_KEY, OUTER => TRUE)) ft \t) fttable  LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_BGT_SKILL_030401\" skill ON skill.BGT_SKILL_030401_KEY = fttable.ftvalue GROUP BY fttable.VACANCY_KEY ), taxonomy as ( SELECT vac.vacancy_key as taxonomy_key ,array_agg(object_construct('LEP',IFF(sub.LOCALENTERPRISEPARTNERSHIP IS NOT NULL AND location.COUNTRY_NAME_EN like 'United Kingdom%',sub.LOCALENTERPRISEPARTNERSHIP,''),'VERSION','3.4'))LEP ,array_agg(object_construct('LAD',IFF(sub.LOCALAUTHORITYDISTRICT IS NOT NULL AND location.COUNTRY_NAME_EN like 'United Kingdom%',sub.LOCALAUTHORITYDISTRICT,''),'VERSION','3.4'))LAD FROM vacancy vac \t LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_LOCATION_010000\" location ON location.LOCATION_010000_KEY = vac.LOCATION_010000_KEY[0] LEFT JOIN  ( SELECT lt.CITY,lt.COUNTY,lt.LOCALENTERPRISEPARTNERSHIP,lt.LOCALAUTHORITYDISTRICT,ROW_NUMBER() OVER(PARTITION BY lt.CITY,lt.COUNTY ORDER BY lt.CITY,lt.COUNTY ASC) as R FROM \"BGT_DEVELOPMENT\".\"NOVA\".\"Location_Taxonomy\" lt  ) sub ON sub.CITY=location.CITY_NAME_EN AND location.COUNTY_REGION_EN=sub.COUNTY AND ALPHA_2_CODE='GB' WHERE sub.R=1 GROUP BY vac.vacancy_key ) \tSELECT ALPHA_2_CODE as Geo,location.STATE_PROVINCE_NAME_EN as State,posting_map.POSTING_KEY, \tOBJECT_CONSTRUCT \t( \t'FEED_VERSION','v1.0' \t,'STATUS','Add' \t,'VACANCY_KEY',vac.VACANCY_KEY::NUMBER (38,0) \t,'FIRST_ACTIVE_DATE',IFF(activedate.DATE is NULL,'',activedate.DATE::string) \t,'LAST_ACTIVE_DATE',IFF(expdate.DATE is NULL,'',expdate.DATE::string) \t,'CERTIFICATION_NAME',certs_map.CERTIFICATION_NAME_EN_US \t,'SOURCE_DOMAIN',IFF(posting_map.SOURCE_DOMAIN IS NULL,'',posting_map.SOURCE_DOMAIN) \t,'ORIGINAL_URL',IFF(posting_map.ORIGINAL_URL IS NULL,'',posting_map.ORIGINAL_URL) \t,'MIN_DEGREE',edu_map_min.EDUCATION_LEVEL_CODE_MIN_DEGREE_LEVEL \t,'MAX_DEGREE',edu_map_max.EDUCATION_LEVEL_CODE_MAX_DEGREE_LEVEL \t,'CIP_CODE',cip_map.CIP6_CODE \t,'NQF_PREFERRED',IFF(nqf_pre_map.NQF_ID_preferred IS NULL, (array_construct(object_construct('NQF_PREFERRED','','NAME','', 'VERSION','2015'))), nqf_pre_map.NQF_ID_preferred) \t,'NQF_REQUIRED',nqf_req_map.NQF_ID_required \t,'EDUCATION_LEVEL',edu_map.EDUCATION_LEVEL_CODE_EDUCATION_LEVEL \t,'EMPLOYER_NAME',array_construct(object_construct('EMPLOYER_NAME',IFF(vac.EMPLOYER_NAME IS NULL,'',vac.EMPLOYER_NAME),'VERSION', '3.4')) \t,'RECRUITER_NAME',array_construct(object_construct('RECRUITER_NAME',IFF(vac.RECRUITER_NAME IS NULL,'',vac.RECRUITER_NAME),'VERSION', '3.4')) \t,'EMPLOYER_TICKER',array_construct(object_construct('EMPLOYER_TICKER',IFF(vac.EMPLOYER_TICKER IS NULL,'',vac.EMPLOYER_TICKER),'VERSION', '3.4')) \t,'EXPERIENCE_MIN_YEARS',IFF(vac.EXPERIENCE_MIN_YEARS IS NULL,parse_json('null'),vac.EXPERIENCE_MIN_YEARS::FLOAT) \t,'EXPERIENCE_MAX_YEARS',IFF(vac.EXPERIENCE_MAX_YEARS IS NULL,parse_json('null'),vac.EXPERIENCE_MAX_YEARS::FLOAT) \t,'YEARS_OF_EXPERIENCE',IFF(exp.EXPERIENCE_CLASS_EN_US IS NULL,'',exp.EXPERIENCE_CLASS_EN_US) \t,'YEARS_OF_EXPERIENCE_LEVEL',IFF(exp.EXPERIENCE_CLASS_CODE IS NULL,parse_json('null'),exp.EXPERIENCE_CLASS_CODE::INT) \t,'UK_SIC_CODE',array_construct(object_construct('UK_SIC_CODE',IFF(uksic.SUBCLASS_KEY IS NULL,'',uksic.SUBCLASS_KEY),'VERSION','2007','NAME',IFF(uksic.subclass_title_en_gb IS NULL,'',uksic.subclass_title_en_gb))) \t,'SGP_SSIC_CODE',array_construct(object_construct('SGP_SSIC_CODE',IFF(sgp_ssic.ITEM_KEY IS NULL,'',sgp_ssic.ITEM_KEY),'VERSION','2015','NAME',IFF(sgp_ssic.item_title IS NULL,'',sgp_ssic.item_title))) \t,'US_CAN_NAICS_CODE',array_construct(object_construct('US_CAN_NAICS_CODE',IFF(us_can_naics.NATIONAL_INDUSTRY_KEY IS NULL,parse_json('null'),us_can_naics.NATIONAL_INDUSTRY_KEY::BIGINT),'VERSION','2012','NAME',IFF(us_can_naics.NATIONAL_INDUSTRY_TITLE_EN_US \t IS NULL,'',us_can_naics.NATIONAL_INDUSTRY_TITLE_EN_US \t))) \t,'ANZ_SIC_CODE',array_construct(object_construct('ANZ_SIC_CODE',IFF(anzsic_2006.CLASS_KEY IS NULL,'',anzsic_2006.CLASS_KEY),'VERSION','2012','NAME',IFF(anzsic_2006.class_title:en IS NULL,'',anzsic_2006.class_title:en))) \t,'INTERNSHIP_FLAG',IFF(terms_of_service_type.TYPE IS NULL,FALSE,IFF(terms_of_service_type.TYPE='intership',TRUE,FALSE)) \t,'WORK_HOURS',array_construct(object_construct('WORK_HOURS',IFF(hours_of_service_type.TYPE IS NULL,'',hours_of_service_type.TYPE),'VERSION','3.4')) \t,'WORK_TYPE',array_construct(object_construct('WORK_TYPE',IFF(terms_of_service_type.TYPE IS NULL,'',terms_of_service_type.TYPE),'VERSION','3.4')) \t,'WORK_FROM_HOME',IFF(vac.WORK_FROM_HOME IS NULL,FALSE,TRUE) \t,'COUNTRY',array_construct(object_construct('COUNTRY',IFF(location.COUNTRY_NAME_EN IS NULL,'',location.COUNTRY_NAME_EN),'VERSION','3.4')) \t,'STATE',array_construct(object_construct('STATE',IFF(location.STATE_PROVINCE_NAME_EN IS NULL,'',location.STATE_PROVINCE_NAME_EN),'VERSION','3.4')) \t,'COUNTY_REGION',array_construct(object_construct('County_REGION',IFF(location.COUNTY_REGION_EN IS NULL,'',location.COUNTY_REGION_EN),'VERSION','3.4')) \t,'CITY',array_construct(object_construct('CITY',IFF(location.CITY_NAME_EN IS NULL,'',location.CITY_NAME_EN),'VERSION','3.4')) \t,'MSA',array_construct(object_construct('MSA',IFF(location.METROPOLITAN_AREA_NAME_EN IS NULL,'',location.METROPOLITAN_AREA_NAME_EN),'VERSION','3.4')) \t,'BGTOCC',array_construct(object_construct('BGTOCC',IFF(bgtocc.OCCUPATION_CODE IS NULL,'',bgtocc.OCCUPATION_CODE),'VERSION','3.4','NAME',IFF(bgtocc.occupation_name_en_us IS NULL,'',bgtocc.occupation_name_en_us)),object_construct('BGTOCC',IFF(bgtocc2.OCCUPATION_CODE IS NULL,'',bgtocc2.OCCUPATION_CODE),'VERSION','3.7','NAME',IFF(bgtocc2.occupation_name_en_us IS NULL,'',bgtocc2.occupation_name_en_us))) \t,'SGP_SOC_OCCUPATION',array_construct(object_construct('SGP_SOC_OCCUPATION',IFF(sgp_soc_occupation.OCCUPATION_CODE IS NULL,'',sgp_soc_occupation.OCCUPATION_CODE),'VERSION','2015','NAME',IFF(sgp_soc_occupation.occupation_title_en_us IS NULL,'',sgp_soc_occupation.occupation_title_en_us))) \t,'ONET',array_construct(object_construct('ONET',IFF(us_onet.ONET IS NULL,'',us_onet.ONET),'VERSION','2010','NAME',IFF(us_onet.ONET_SOC_OCCUPATION_EN_US IS NULL,'',us_onet.ONET_SOC_OCCUPATION_EN_US))) \t,'UK_SOC_OCCUPATION',array_construct(object_construct('UK_SOC_OCCUPATION',IFF(uk_soc_occupation.UNIT_GROUP_KEY IS NULL,'',uk_soc_occupation.UNIT_GROUP_KEY),'VERSION','2010','NAME',IFF(uk_soc_occupation.unit_group_title_en_us IS NULL,'',uk_soc_occupation.unit_group_title_en_us))) \t,'ANZ_SCO_OCCUPATION',array_construct(object_construct('ANZ_SCO_OCCUPATION',IFF(anz_sco_occupation.OCCUPATION_CODE IS NULL,'',anz_sco_occupation.OCCUPATION_CODE),'VERSION','2010','NAME',IFF(anz_sco_occupation.occupation_title_en_us IS NULL,'',anz_sco_occupation.occupation_title_en_us))) \t,'CAN_NOC_OCCUPATION',array_construct(object_construct('CAN_NOC_OCCUPATION',IFF(can_noc_occupation.UNIT_GROUP_OCCUPATION_CODE IS NULL,'',can_noc_occupation.UNIT_GROUP_OCCUPATION_CODE),'VERSION','2011','NAME',IFF(can_noc_occupation.unit_group_occupation_title_en_us IS NULL,'',can_noc_occupation.unit_group_occupation_title_en_us))) \t,'US_SUBOCC',array_construct(object_construct('US_SUBOCC',IFF(bgtocc.SUB_OCCUPATION_NAME_EN_US IS NULL,'',bgtocc.SUB_OCCUPATION_NAME_EN_US),'VERSION','3.4'),object_construct('US_SUBOCC',IFF(bgtocc2.SUB_OCCUPATION_NAME_EN_US IS NULL,'',bgtocc2.SUB_OCCUPATION_NAME_EN_US),'VERSION','3.7')) \t,'MAX_ANNUAL_SALARY',IFF(vac.advertised_max_salary_annual_usd IS NULL,parse_json('null'),ROUND(vac.advertised_max_salary_annual_usd::FLOAT,2)) \t,'MIN_ANNUAL_SALARY',IFF(vac.advertised_min_salary_annual_usd IS NULL,parse_json('null'),ROUND(vac.advertised_min_salary_annual_usd::FLOAT,2)) \t,'MAX_HOURLY_SALARY',IFF(vac.advertised_max_salary_hourly_usd IS NULL,parse_json('null'),ROUND(vac.advertised_max_salary_hourly_usd::FLOAT,2)) \t,'MIN_HOURLY_SALARY',IFF(vac.advertised_min_salary_hourly_usd IS NULL,parse_json('null'),ROUND(vac.advertised_min_salary_hourly_usd::FLOAT,2)) \t,'PREDICTED_SALARY',IFF(vac.MARKET_MIN_SALARY_LOCAL IS NULL,parse_json('null'),vac.MARKET_MIN_SALARY_LOCAL::FLOAT) \t,'SKILL',skill_map.SKILL \t,'SOURCE',posting_map.SOURCE \t,'TITLE_DISPLAY_TEXT',IFF(vac.TITLE_DISPLAY_TEXT IS NULL,'',vac.TITLE_DISPLAY_TEXT) \t,'LABOUR_MARKET_REGION',array_construct(object_construct('LABOUR_MARKET_REGION',IFF((location.metropolitan_area_name_en IS NOT NULL AND (location.COUNTRY_NAME_EN ='Australia' OR location.COUNTRY_NAME_EN='New Zealand')),location.metropolitan_area_name_en,''),'VERSION','3.4')) \t,'SA4CODE', array_construct(object_construct('SA4CODE',IFF((location.COUNTY_REGION_EN IS NOT NULL AND (location.COUNTRY_NAME_EN ='Australia' OR location.COUNTRY_NAME_EN='New Zealand')),location.COUNTY_REGION_EN,''),'VERSION','3.4')) \t,'WORK_AREA',array_construct(object_construct('WORK_AREA',IFF((location.metropolitan_area_name_en IS NOT NULL AND location.COUNTRY_NAME_EN like 'United Kingdom%'),location.metropolitan_area_name_en,''),'VERSION','3.4')) \t,'LAD',IFF(taxonomy.LAD IS NULL, (array_construct(object_construct('LAD','','VERSION','3.4'))), taxonomy.LAD) \t,'LEP',IFF(taxonomy.LEP IS NULL, (array_construct(object_construct('LEP','','VERSION','3.4'))), taxonomy.LEP) \t) as Job FROM vacancy vac  LEFT JOIN posting_map ON posting_map.POSTING_MAP_KEY = vac.VACANCY_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_DATE\" activedate ON vac.FIRST_ACTIVE_DATE_KEY=activedate.DATE_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_DATE\" expdate ON vac.LAST_ACTIVE_DATE_KEY=expdate.DATE_KEY LEFT JOIN certs_map ON certs_map.CERTS_MAP_KEY = vac.VACANCY_KEY LEFT JOIN edu_map ON edu_map.EDU_MAP_KEY = vac.VACANCY_KEY LEFT JOIN cip_map ON cip_map.CIP_MAP_KEY = vac.VACANCY_KEY  LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_EXPERIENCE\" exp ON exp.EXPERIENCE_KEY=vac.EXPERIENCE_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_UKSIC_2007\" uksic ON uksic.UKSIC_2007_KEY = vac.UKSIC_2007_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_SSIC_2015\" sgp_ssic ON sgp_ssic.SSIC_2015_KEY = vac.SSIC_2015_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_NAICS_2012\" us_can_naics ON us_can_naics.NAICS_2012_KEY = vac.NAICS_2012_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_ANZSIC_2006\" anzsic_2006 ON anzsic_2006.ANZSIC_2006_KEY = vac.ANZSIC_2006_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_TERMS_OF_SERVICE_TYPE\" terms_of_service_type ON terms_of_service_type.TERMS_OF_SERVICE_TYPE_KEY = vac.TERMS_OF_SERVICE_TYPE_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_HOURS_OF_SERVICE_TYPE\" hours_of_service_type ON hours_of_service_type.HOURS_OF_SERVICE_TYPE_KEY = vac.HOURS_OF_SERVICE_TYPE_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_BGT_OCCUPATION_030401\" bgtocc ON bgtocc.BGT_OCCUPATION_030401_KEY=vac.BGT_OCCUPATION_030401_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_SG_SSOC_OCCUPATION_2015\" sgp_soc_occupation ON sgp_soc_occupation.SG_SSOC_OCCUPATION_2015_KEY=vac.SG_SSOC_OCCUPATION_2015_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_ONET_OCCUPATION_2010\" us_onet ON us_onet.ONET_OCCUPATION_2010_KEY=vac.ONET_OCCUPATION_2010_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_GB_SOC_OCCUPATION_2010\" uk_soc_occupation ON uk_soc_occupation.GB_SOC_OCCUPATION_2010_KEY=vac.GB_SOC_OCCUPATION_2010_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_AU_ANZSCO_OCCUPATION_2013\" anz_sco_occupation ON anz_sco_occupation.AU_ANZSCO_OCCUPATION_2013_KEY=vac.AU_ANZSCO_OCCUPATION_2013_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_CA_NOC_OCCUPATION_2011\" can_noc_occupation ON can_noc_occupation.CA_NOC_OCCUPATION_2011_KEY=vac.CA_NOC_OCCUPATION_2011_KEY LEFT JOIN skill_map ON skill_map.SKILL_MAP_KEY = vac.VACANCY_KEY LEFT JOIN edu_map_min ON edu_map_min.EDU_MAP_KEY = vac.VACANCY_KEY LEFT JOIN edu_map_max ON edu_map_max.EDU_MAP_KEY = vac.VACANCY_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_LOCATION_010000\" location ON location.LOCATION_010000_KEY = vac.LOCATION_010000_KEY[0] LEFT JOIN taxonomy ON taxonomy.taxonomy_key = vac.VACANCY_KEY LEFT JOIN \"BGT_DATA_34\".\"PUBLIC\".\"D_BGT_OCCUPATION_030702\" bgtocc2 ON bgtocc2.BGT_OCCUPATION_030702_KEY=vac.BGT_OCCUPATION_030702_KEY LEFT JOIN nqf_req_map ON nqf_req_map.NQF_MAP_KEY = vac.VACANCY_KEY LEFT JOIN nqf_pre_map ON nqf_pre_map.NQF_MAP_KEY = vac.VACANCY_KEY",
                    GeoFeedFileNameTemplate="Baseline_{Geo}_VacancySuperFeed_{FeedStartDate}_JobsFor_{FeedStartDate}_{FeedStartHour}-{FeedEndHour}",
                    StateFeedFileNameTemplate="Baseline_{Geo}_{State}_VacancySuperFeed_{FeedStartDate}_JobsFor_{FeedStartDate}_{FeedStartHour}-{FeedEndHour}",
                    FeedTempPath="s3://nov-app-dev-poc-bgcloud-s3/Glue-Exports/SampleFeedOutput/glue-output.json",
                    GeoSFTPPath="s3://bgt-sftp-poc-home/DailyFeeds/json-glue/{Geo}/",
                    StateSFTPPath="s3://bgt-sftp-poc-home/DailyFeeds/json-glue/{Geo}/{State}/",
                    SnowflakeURL="wv35338.us-east-1.snowflakecomputing.com", SnowflakeAccount="wv35338",
                    SnowflakeAccountSecretmanager="nova/snowflake_user_bgt_nova_development_agent",
                    SnowflakeDatabase="BGT_DATA_34", SnowflakeSchema="PUBLIC",
                    SnowflakeRole="BGT_NOVA_SERVICE_DEVELOPMENT", SnowflakeWarehouse="BGT_ATLAS",
                    FeedStartDate="2020-08-01", FeedEndDate="2020-08-07", IsDaily=0, Span=6, BaseHour="00:00",
                    FeedType="HISTORIC", DateType="DATE", FeedDelay=28,
                    SparkSQLQuery="select json_udf(JOB,'JOBID',case when source_id is null then '' else source_id end ,'RAW_TEXT',case when record_data.value is null then '' else record_data.value end) as JOB, GEO, STATE from SnowflakeData s left join source so on so.posting_key = s.POSTING_KEY left join elab e on e.posting_key = s.POSTING_KEY",
                    s3DataFrames="s3://bgt-atlas-datalake-dev/data/jobpostings/V030401/sc_jobposting_source;source|s3://bgt-atlas-datalake-dev/data/jobpostings/V030401/sc_jobposting_elab;elab|s3://bgt-atlas-datalake-dev/data/jobpostings/V030401/sc_jobposting_rawdata;rawdata")

class botoClientMock:
    def client(self):
        return botoClient()

class botoClient:
    def __init__(self,appconfig):
         True
    def get_configuration(self,Application,Environment,Configuration,ClientId,ClientConfigurationVersion):
        return {'Content': response()}
    def copy(self,CopySource,Bucket,Key):
        return True
    def list_objects_v2(self,Bucket,Prefix,MaxKeys):
        response ={'Contents':[{'Key':'string.txt'}]}
        return response
    def upload_fileobj(self,Fileobj,Bucket,Key,ExtraArgs):
        return True

class botoResource:
    def __init__(self,appconfig):
         True

    def Bucket(self,source_bucket):
        response ={'Key':'string.txt'}
        return bucket()

class bucket:
    def Object(self,key):
        return obj()

class obj:
    def get(self):
        return {'Body':response()}


class response:

    def read(self):
        return json.dumps(sampleconfig)

class response:

    def read(self):
        return json.dumps(sampleconfig)

class sparkSessionbuilder:
    def __init__(self, key, value):
       True

    def getOrCreate(self):
        return sparkOperations()


class sparkOperations:
    def __init__(self):
        self.udf = udfRegister()
        self.read = sparkread()

    def sql(self,query):
        return dataframe()

class sparkread:
    def format(self,source):
        return self
    def options(self,sfURL,sfAccount,sfUser,sfPassword,sfWarehouse,sfDatabase,sfSchema,sfRole):
        return self
    def option(self,query, snowflakeQuery):
        return self
    def load(self,path= ''):
        return dataframe()


class udfRegister:
    def register(self, udf, json):
        return True

class shutil:
    def __init__(self,response,gz):
        True

class boto3Session:
    def __init__(self):
        True
    def client(self,service_name, region_name):
        return secretValue()

class secretValue:
    def get_secret_value(self,SecretId):
        if(SecretId == "nova/snowflake_user_bgt_nova_development_agent"):
            return {'SecretString': '{\"USER\":\"user\",\"PASSWORD\":\"pass\"}'}
        elif(SecretId == "DecryptionFailureException"):
            raise ClientError({'Error':{'Code':'DecryptionFailureException'}}, 'DecryptionFailureException')
        elif(SecretId == "InternalServiceErrorException"):
            raise ClientError({'Error':{'Code':'InternalServiceErrorException'}}, 'InternalServiceErrorException')
        elif(SecretId == "InvalidParameterException"):
            raise ClientError({'Error':{'Code':'InvalidParameterException'}}, 'InvalidParameterException')
        elif(SecretId == "InvalidRequestException"):
            raise ClientError({'Error':{'Code':'InvalidRequestException'}}, 'InvalidRequestException')
        elif(SecretId == "ResourceNotFoundException"):
            raise ClientError({'Error':{'Code':'ResourceNotFoundException'}}, 'ResourceNotFoundException')


class awsglueArguments:
    @staticmethod
    def getResolvedOptions(args, options):
        return {'s3Bucket':'s3Bucket','ConfigPath':'s3ConfigPath','application_name':'Superfeed','environment_name':'DEV','configuration_name':'DailyVacancy','client_configuration_version':'1'}

class dataframe:

    def __init__(self):
        self.rdd = self
        self.write = self
    def count(self):
        return 1
    def createOrReplaceTempView(self, Viewname):
        return True
    def select(self,*args):
        return self
    def distinct(self):
        return self
    def map(self,object):
        return self
    def collect(self):
        return [('US', 'Indiana'), ('US', 'Massachusetts'), ('US', 'West Virginia'), ('US', 'Idaho'), ('US', 'Vermont'), (None, None), ('SG', None), ('US', 'Michigan'), ('AU', 'Tasmania')]
    def zipWithIndex(self):
        return self
    def toDF(self):
        return self
    def coalesce(self,num):
        return self
    def format(self,type):
        return self
    def option(self,key,value):
        return self
    def mode(self,key):
        return self
    def save(self,file):
        return True


@patch('boto3.client', botoClient)
@patch('boto3.resource', botoResource)
@patch('boto3.session.Session', boto3Session)
@patch('pyspark.sql.SparkSession.builder.config', sparkSessionbuilder)
@patch('shutil.copyfileobj', shutil)
@patch('awsglue.utils', awsglueArguments)

class TestSuperFeedExport(unittest.TestCase):
    def test_createSparkSession(self):
        print('Initializing Spark Session')
        obj = SuperFeedExport()
        obj.createSparkSession()

    def test_loadConfiguration(self):
        application_name='NOV-APP-DEV-POC-BGCLOUD'
        environment_name='DEV'
        configuration_name='DailyVacancy'
        client_configuration_version='2'
        client = boto3.client('appconfig')
        response = client.get_configuration(
            Application=application_name,
            Environment=environment_name,
            Configuration=configuration_name,
            ClientId='test',
            ClientConfigurationVersion=client_configuration_version
        )
        self.assertTrue(response)

    def test_loadConfiguration_s3(self):
        obj=SuperFeedExport()
        obj.load_configuration()

    def test_driver_historic(self):
        sampleconfig['FeedType'] = 'Historic'
        sampleconfig['DateType'] = 'DATE'
        obj=SuperFeedExport()
        obj.driver()

    def test_driver_delta(self):
        sampleconfig['FeedType'] = 'delta'
        sampleconfig['DateType'] = 'DATE'
        obj=SuperFeedExport()
        obj.driver()

    def test_driver_baseline(self):
        sampleconfig['FeedType'] = 'baseline'
        sampleconfig['DateType'] = 'DATE'
        obj=SuperFeedExport()
        obj.driver()

    def test_driver_historic_datetime(self):
        sampleconfig['FeedType'] = 'Historic'
        sampleconfig['DateType'] = 'DATETIME'
        obj=SuperFeedExport()
        obj.driver()

    def test_driver_delta_datetime(self):
        sampleconfig['FeedType'] = 'delta'
        sampleconfig['DateType'] = 'DATETIME'
        obj=SuperFeedExport()
        obj.driver()


    def test_copyFileToS3(self):
        source_bucket = "source_bucket"
        target_bucket = "target_bucket"
        source_prefix = "source_prefix"
        target_prefix = "target_prefix"
        obj=SuperFeedExport()
        obj.copyFileToS3(source_bucket,target_bucket,source_prefix,target_prefix)

    def test_renameOutputFile(self):
        source_bucket = "source_bucket"
        target_bucket = "target_bucket"
        source_prefix = "source_prefix"
        target_prefix = "target_prefix"
        obj=SuperFeedExport()
        obj.renameOutputFile(source_bucket,target_bucket,source_prefix,target_prefix)

    def test_compressFile(self):
        source_bucket = "source_bucket"
        target_bucket = "target_bucket"
        source_prefix = "source_prefix"
        target_prefix = "target_prefix"
        fileName = "fileName"
        obj=SuperFeedExport()
        obj.compressFile(source_bucket, target_bucket, source_prefix, target_prefix, fileName)

    def test_registerUDF(self):
        obj = SuperFeedExport()
        obj.createSparkSession()
        obj.registerUDF()

    def test_get_secret_DecryptionFailureException(self):
        obj = SuperFeedExport()
        with self.assertRaises(Exception) as context:
            obj.get_secret("DecryptionFailureException")

    def test_get_secret_InternalServiceErrorException(self):
        obj = SuperFeedExport()
        with self.assertRaises(Exception) as context:
            obj.get_secret("InternalServiceErrorException")

    def test_get_secret_InvalidParameterException(self):
        obj = SuperFeedExport()
        with self.assertRaises(Exception) as context:
            obj.get_secret("InvalidParameterException")

    def test_get_secret_InvalidRequestException(self):
        obj = SuperFeedExport()
        with self.assertRaises(Exception) as context:
            obj.get_secret("InvalidRequestException")

    def test_get_secret_ResourceNotFoundException(self):
        obj = SuperFeedExport()
        with self.assertRaises(Exception) as context:
            obj.get_secret("ResourceNotFoundException")

if __name__ == '__main__':
    if is_running_under_teamcity():
        runner = TeamcityTestRunner()
    else:
        runner = unittest.TextTestRunner()
    unittest.main(testRunner=runner)